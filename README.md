# Expose Mafia - dockerized

Builds container image for

https://github.com/acajic/Mafia (Rails Server) and https://github.com/acajic/MafiaFront (Angular Client)

to make it easy to deploy your own mafia service to play with (soon to be ex-)friends.

This Repo also provides `docker-compose.yml` with the required DB (+configuration)

## Usage

- clone this repo
- edit the database credentials in `docker-compose.yml` and `server/config/database.yml`
- `docker-compose pull`
- `docker-compose run mafia-server rake db:setup RAILS_ENV=production`
- `docker-compose up -d`
- Visit `localhost:80` and create your Super Admin user
- invite your friends

